<?php

/**
 * Form callback for settings form
 */
function adtoma_fusion_form_settings() {
  $form['config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default Configuration Settings'),
  );
  
  $config = adtoma_fusion_new_config_ob();
  
  foreach ($config->data['config'] as $key => $default) {
    $form['config']['adtoma_fusion_config_default_'. $key] = array(
      '#type' => 'textfield',
      '#title' => $key,
      '#default_value' => $default,
    );
  }
  
  $form['adtoma_fusion_test'] = array(
    '#type' => 'checkbox',
    '#title' => t('Testing ON'),
    '#suffix' => t('This setting adds a background to all the adtoma fusion spaces currently on the site. Do not enable on a production environment.'),
    '#default_value' => variable_get('adtoma_fusion_test', FALSE),
  );
  
  return system_settings_form($form);
}