<?php

/**
 * @file
 *   Contains features hooks for adtoma fusion module
 */

###########################
## SPACE COMPONENT HOOKS ##
###########################

/**
 * Implement hook_features_export
 */
function adtoma_fusion_space_features_export($data, &$export, $module_name) {
  foreach ($data as $item) {
    if (!isset($export['features']['adtoma_fusion_space'])) {
      $export['features']['adtoma_fusion_space'] = array();
      $export['dependencies']['token'] = 'token';
      $export['dependencies']['adtoma_fusion'] = 'adtoma_fusion';
    }
    $export['features']['adtoma_fusion_space'][$item] = $item;
  }
}

/**
 * Implement hook_features_export_options
 */
function adtoma_fusion_space_features_export_options() {
  $options = array();
  $spaces = adtoma_fusion_space_load_multiple();
  foreach ($spaces as $space) {
    $options[$space->name] = $space->description ? "$space->description ($space->name)" : $space->name;
  }
  return $options;
}

/**
 * Implement hook_features_export_render
 */
function adtoma_fusion_space_features_export_render($module_name, $data, $export = NULL) {
  $code = array();
  $code[] = '  $spaces = array();' . "\n";
  foreach ($data as $name) {
    $space = adtoma_fusion_space_load_from_name($name);
    if (empty($space->name)) {
      // Something has gone wrong
      continue;
    }
    unset($space->id); // We dont want to export the primary key
    $code[] = "  /**\n   * Export data for space: " . $space->name . "\n   */";
    $code[] = '  $spaces["'. $name .'"] = ' . features_var_export($space) . ';';
  }
  $code[] = "\n" . 'return $spaces;';

  $rendered_code = implode("\n", $code);
  return array('adtoma_fusion_default_spaces' => $rendered_code);
}

/**
 * Implement hook_features_revert
 */
function adtoma_fusion_space_features_revert($module_name) {
  adtoma_fusion_space_features_rebuild($module_name);
}

/**
 * Implement hook_features_rebuild
 */
function adtoma_fusion_space_features_rebuild($module_name) {
  // Features hasn't loaded our file by this stage, so force it to load. (This perhaps should be unnecessary.)
  // module_load_include('inc', $module_name, "$module_name.features.adtoma_fusion_space");

  // Load the defaults straight from file and same them to db.
  $defaults = module_invoke($module_name, 'adtoma_fusion_default_spaces');
  if (!empty($defaults)) {
    foreach ($defaults as $name => $space) {
      $space = (object)$space;
      unset($space->id);
      adtoma_fusion_space_delete_from_name($space->name);
      adtoma_fusion_space_save($space);
    }
  }
}


############################
## CONFIG COMPONENT HOOKS ##
############################

/**
 * Implement hook_features_export
 */
function adtoma_fusion_configuration_features_export($data, &$export, $module_name) {
  foreach ($data as $item) {
    if (!isset($export['features']['adtoma_fusion_configuration'])) {
      $export['features']['adtoma_fusion_configuration'] = array();
      $export['dependencies']['token'] = 'token';
      $export['dependencies']['adtoma_fusion'] = 'adtoma_fusion';
    }
    $export['features']['adtoma_fusion_configuration'][$item] = $item;
  }
}

/**
 * Implement hook_features_export_options
 */
function adtoma_fusion_configuration_features_export_options() {
  $options = array();
  $configurations = adtoma_fusion_config_load_multiple();
  foreach ($configurations as $configuration) {
    $options[$configuration->machine_name] = $configuration->description ? "$configuration->description ($configuration->machine_name)" : $configuration->machine_name;
  }
  return $options;
}

/**
 * Implement hook_features_export_render
 */
function adtoma_fusion_configuration_features_export_render($module_name, $data, $export = NULL) {
  $code = array();
  $code[] = '  $configuration = array();' . "\n";
  foreach ($data as $name) {
    $configuration = adtoma_fusion_config_load($name);
    if (!empty($configuration)) {
      unset($configuration->id);
      $code[] = "  /**\n   * Export data for configuration: " . $configuration->machine_name . "\n   */";
      $code[] = '  $configuration["'. $name .'"] = ' . features_var_export($configuration) . ';';
    }
  }
  $code[] = "\n" . 'return $configuration;';

  $rendered_code = implode("\n", $code);
  return array('adtoma_fusion_default_configurations' => $rendered_code);
}

/**
 * Implement hook_features_revert()
 */
function adtoma_fusion_configuration_features_revert($module_name) {
  adtoma_fusion_configuration_features_rebuild($module_name);
}

/**
 * Implement hook_features_rebuild
 */
function adtoma_fusion_configuration_features_rebuild($module_name) {
  // Features hasn't loaded our file by this stage, so force it to load. (This perhaps should be unnecessary.)
  // module_load_include('inc', $module_name, "$module_name.features.adtoma_fusion_configuration");

  // Load and save the configs from file and same them.
  $defaults = module_invoke($module_name, 'adtoma_fusion_default_configurations');
  if (!empty($defaults)) {
    foreach ($defaults as $name => $config) {
      $config = (object)$config;
      unset($config->id);
      try {
        adtoma_fusion_config_delete($config->machine_name);
        adtoma_fusion_config_save($config);
      }
      catch (Exception $e) {
        throw new Exception(t('Could not revert adtoma_fusion_config: !name', array('!name' => $config->machine_name)));
      }
    }
  }
}
