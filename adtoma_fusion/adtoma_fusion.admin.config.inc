<?php

##
## CONFIGURATION ADMIN 
##

//------------------------------------------------------------------------------
// Adtoma Fusion Configuration overview

/**
 * Page callback for configuration overview
 */
function adtoma_fusion_page_overview_configs() {
  $configs = adtoma_fusion_config_load_multiple();
  $vars['header'] = array(
    'Weight', // Weight tabledrag column
    'Label',
    'Description',
    'Actions',
    'Enabled',
  );
  
  foreach ($configs as $config) {
    $edit_url = 'admin/adtoma/fusion/config/'. $config->machine_name .'/edit';
    $delete_url = 'admin/adtoma/fusion/config/'. $config->machine_name .'/delete';
    $actions = array(
      l('edit', $edit_url), 
      l('delete', $delete_url),
    );
    
    $vars['rows'][] = array(
      'data' => array(
        'weight' => $config->weight,
        'label' => l($config->label, $edit_url),
        'description' => $config->description,
        'actions' => implode(', ', $actions),
        'enabled' => (empty($config->disabled)) ? 'Yes' : 'No',
      ),
    );
  }
  
  return theme('table', $vars);
}

//------------------------------------------------------------------------------
// Adtoma Fusion Configuration add section

/**
 * Form callback for add configuration page
 */
function adtoma_fusion_form_add_config($form, &$form_state) {
  // Create a default config object
  $config = adtoma_fusion_new_config_ob();
  $form['#submit'][] = 'adtoma_fusion_form_add_config_submit';
  
  return adtoma_fusion_form_config($form, $form_state, $config);
}

/**
 * Submit handler for config add form
 */
function adtoma_fusion_form_add_config_submit($form, &$form_state) {
  $config = $form_state['#adtoma_fusion_config'];
  if ($config->id) {
    drupal_set_message(t('Successfully added configuration %label!', array('%label' => $config->label)));
  }
}

//------------------------------------------------------------------------------
// Adtoma Fusion Configuration edit section

/**
 * Form callback for config edit form
 */
function adtoma_fusion_form_edit_config($form, &$form_state, $config) {
  $form = adtoma_fusion_form_config($form, $form_state, $config);

  $form['#submit'][] = 'adtoma_fusion_form_edit_config_submit';

  $arg = arg();
  array_pop($arg);
  $arg[] = 'delete';
  $form['buttons']['delete'] = array(
    '#markup' => l('Delete', implode('/', $arg)),
  );
  
  return $form;
}

/**
 * Submission handler for config edit form
 */
function adtoma_fusion_form_edit_config_submit($form, &$form_state) {
  $config = $form_state['#adtoma_fusion_config'];
  drupal_set_message(t('Successfully updated configuration %label!', array('%label' => $config->label)));
}

//------------------------------------------------------------------------------
// Adtoma Fusion Configuration delete section

/**
 * Form callback for config delete form
 */
function adtoma_fusion_form_delete_config($form, &$form_state, $config) {
  $form['machine_name'] = array(
    '#type' => 'hidden',
    '#value' => $config->machine_name,
  );
  $form['#submit'] = array('adtoma_fusion_form_delete_config_submit');
  
  $question = t('Are you sure you wish to delete %label?', array('%label' => $config->label));
  $arg = arg();
  $path = implode('/', array_slice($arg, 0, count($arg) - 2));
  return confirm_form($form, $question, $path);
}

/**
 * Submission handler for config delete form
 */
function adtoma_fusion_form_delete_config_submit($form, &$form_state) {
  $machine_name = $form_state['values']['machine_name'];
  $config = adtoma_fusion_config_load($machine_name);
  adtoma_fusion_config_delete($machine_name);
  drupal_set_message(t('Successfully deleted %label!', array('%label' => $config->label)));
  
  $arg = arg();
  $form_state['redirect'] = implode('/', array_slice($arg, 0, count($arg) - 2));
}

//------------------------------------------------------------------------------
// Adtoma Fusion Configuration shared components
/**
 * Shared form callback for config add/edit pages
 */
function adtoma_fusion_form_config($form, &$form_state, $config) {
  $form['buttons'] = array(
    '#weight' => 10,
    'save' => array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#submit' => array('adtoma_fusion_form_config_submit'),
    ),
  );
  
  $form_state['adtoma_fusion_config'] = $config;
  
  $form['adtoma_fusion_config'] = array(
    '#type' => 'hidden',
    '#value' => TRUE,
  );
  
  $form['#attached']['css'][] = drupal_get_path('module', 'adtoma_fusion') .'/adtoma_fusion.admin.css';
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => 'Label',
    '#description' => t('Human-readable name for this configuration'),
    '#required' => TRUE,
    '#default_value' => $config->label,
  );
  
  $form['machine_name'] = array(
    '#type' => 'machine_name',
    '#machine_name' => array(
      'source' => array('label'),
      'exists' => 'adtoma_fusion_config_machine_name_exists',
    ),
    '#default_value' => $config->machine_name,
  );
  // We have to use the readonly rather than disabled attribute so that the machine name is passed to the ctools access dialog $form_state variable
  $form['machine_name']['#attributes']['readonly'] = ($config->machine_name) ? TRUE : FALSE;
  $form['#attributes']['class'][] = ($config->machine_name) ? 'machine-name-set' : 'machine-name-not-set';
  
  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#description' => t('A brief description of what this configuration does'),
    '#default_value' => $config->description,
  );
  
  $form['disabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disabled'),
    '#default_value' => $config->disabled,
  );
  
  $form['weight'] = array(
    '#type' => 'select',
    '#title' => t('Weight'),
    '#description' => t('Configuration\'s weight when determining visibility'),
    '#options' => array(),
    '#default_value' => $config->weight,
  );
  for ($i = -10; $i <= 10; $i++) {
    $form['weight']['#options'][$i] = $i;
  }
  
  // Config data
  $form['data']['#tree'] = TRUE;
  
  $form['data']['config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
  );
  foreach ($config->data['config'] as $key => $value) {
    $form['data']['config'][$key] = array(
      '#type' => 'textfield',
      '#title' => $key,
      '#default_value' => $value,
    );
  }
  
  $form['data']['kv_pairs'] = array(
    '#type' => 'fieldset',
    '#title' => t('Key-Value Pairs'),
    '#weight' => 3,
  );
  $kv_count = (isset($config->data['kv_pairs'])) ? count($config->data['kv_pairs']) : 1;
  for ($i = 0; $i < ($kv_count + 1); $i++) {
    $form['data']['kv_pairs'][] = array(
      'key' => array(
        '#title' => t('Key'),
        '#type' => 'textfield',
        '#size' => 20,
        '#default_value' => (!empty($config->data['kv_pairs'][$i]['key'])) ? $config->data['kv_pairs'][$i]['key'] : '',
        '#prefix' => '<div class="kv-pair clearfix">',
        '#field_prefix' => $i .':',
        '#field_suffix' => '=&gt;',
      ),
      'value' => array(
        '#title' => t('Value'),
        '#type' => 'textfield',
        '#size' => 20,
        '#default_value' => (!empty($config->data['kv_pairs'][$i]['value'])) ? $config->data['kv_pairs'][$i]['value'] : '',
      ),
      'type' => array(
        '#type' => 'select',
        '#title' => t('Type'),
        '#options' => array(
          'string' => t('Normal'),
          'array' => t('Multi value - Seperate values with comma(s)'),
          'eval' => t('JS Evaluation'),
        ),
        '#default_value' => (!empty($config->data['kv_pairs'][$i]['type'])) ? $config->data['kv_pairs'][$i]['type'] : 'string',
        '#suffix' => '</div>',
      ),
    );
  }
  $form['data']['token'] = array(
    '#type' => 'fieldset',
    '#title' => t('Available tokens'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    'tokens' => array(
      '#theme' => 'token_tree',
      '#token_types' => array('user', 'node'),
    ),
    '#weight' => 100,
  );
  
  if (empty($config->machine_name)) {
    // If we are adding a machine name we have to redirect to the edit page
    $form['buttons']['save']['#value'] = t('Create new configuration');
    $form['buttons']['save']['#submit'][] = 'adtoma_fusion_form_config_submit_redirect_editpage';
  } else {
    // ---------------------------------
    // Only add access configuration once we have done the insert
    
    // Using the ctools access criteria stuff
    $form['access'] = array(
      '#type' => 'fieldset',
      '#title' => t('Selection criteria'),
      'inner' => array(),
    );

    ctools_include('context-access-admin');
    $form_state += array(
      'access' => (!empty($config->data['access'])) ? $config->data['access'] : array(),
      'module' => 'adtoma_fusion',
      'callback argument' => 'config*'. $config->machine_name,
      'contexts' => (!empty($config->data['contexts'])) ? $config->data['contexts'] : array(),
      'adtoma_fusion_config_machine_name' => $config->machine_name,
      'no buttons' => TRUE,
    );
    $empty_form = array();
    $form['access']['inner'] = ctools_access_admin_form($empty_form, $form_state);
  }
  
  return $form;
}

/**
 * Submission handler for shared config form
 */
function adtoma_fusion_form_config_submit($form, &$form_state) {
  $properties = array('label', 'machine_name', 'description', 'weight', 'disabled', 'data');
  $config = new stdClass;
  foreach ($properties as $property) {
    $config->{$property} = (!empty($form_state['values'][$property])) ? $form_state['values'][$property] : 0;
  }

  $argument = 'config*'. $form_state['values']['machine_name'];
  list($config->data['access'], $config->data['contexts']) = adtoma_fusion_ctools_access_get($argument);
  $config->data['access']['logic'] = $form_state['values']['logic'];
  adtoma_fusion_ctools_access_clear($argument);

  adtoma_fusion_config_save($config);
  
  $form_state['#adtoma_fusion_config'] = $config;
  
  // Redirecting to the overview page, shortcutting for possible future URL 
  //changes by simply removing the last argument.
  $arg = arg();
  $form_state['redirect'] = implode('/', array_slice($arg, 0, count($arg) - 1));
}

/**
 * Submit handler which redirects users back to the config edit form after creating a new one
 */
function adtoma_fusion_form_config_submit_redirect_editpage($form, &$form_state) {
  $form_state['redirect'] = 'admin/adtoma/fusion/config/'. $form_state['values']['machine_name'] .'/edit';
  drupal_set_message(t('You are now able to add the access criteria for this configuration.'));
}

/**
 * Title callback for config pages
 */
function adtoma_fusion_title_config($op, $config) {
  return t(ucfirst($op) .' %label', array('%label' => $config->label));
}

