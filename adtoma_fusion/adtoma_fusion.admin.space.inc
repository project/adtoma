<?php

##
## SPACES ADMIN 
##

/**
 * Page callback for spaceuration overview
 */
function adtoma_fusion_page_overview_space() {
  $spaces = adtoma_fusion_space_load_multiple();
  
  $tables = array();
  foreach ($spaces as $space) {
    $edit_url = 'admin/adtoma/fusion/space/'. $space->id .'/edit';
    $delete_url = 'admin/adtoma/fusion/space/'. $space->id .'/delete';
    $actions = array(
      l('edit', $edit_url), 
      l('delete', $delete_url),
    );
    
    $tables[$space->category]['rows'][] = array(
      'data' => array(
        'name' => l($space->name, $edit_url),
        'description' => $space->description,
        'block' => (empty($space->block)) ? 'No' : 'Yes',
        'enabled' => (empty($space->disabled)) ? 'Yes' : 'No',
        'actions' => implode(', ', $actions),
      ),
    );
  }
  
  $output = '';
  foreach ($tables as $category => $vars) {
    $vars['header'] = array(
      'Name', // Weight tabledrag column
      'Description',
      'Block',
      'Enabled',
      'Actions',
    );
    $output .= '<h3>'. $category .'</h3>'. theme('table', $vars);
  }
  
  return $output;
}

/**
 * Form callback for add space page
 */
function adtoma_fusion_form_add_space($form, &$form_state) {
  $space = adtoma_fusion_new_space_ob();
  $form = adtoma_fusion_form_space($form, $form_state, $space);
  return $form;
}

/**
 * Edit form for spaces
 */
function adtoma_fusion_form_edit_space($form, &$form_state, $space) {
  $form = adtoma_fusion_form_space($form, $form_state, $space);
  
  $arg = arg();
  array_pop($arg);
  $arg[] = 'delete';
  $form['buttons']['delete'] = array(
    '#markup' => l('Delete', implode('/', $arg)),
  );
  
  return $form;
}

/**
 * Shared form for space add / edit
 */
function adtoma_fusion_form_space($form, &$form_state, $space) {
  $form['space']['#tree'] = TRUE;
  $form['space']['name'] = array(
    '#title' => t('Name'),
    '#description' => t('The name used for this space\'s ad call.'),
    '#type' => 'textfield',
    '#default_value' => $space->name,
    '#required' => TRUE,
    '#element_validate' => (empty($space->id)) ? array('adtoma_fusion_form_space_name_validate') : array(),
  );
  if (!empty($space->name)) {
    $form['space']['name']['#attributes']['readonly'] = 'readonly';
    $form['space']['name']['#description'] .= ' Once set, this can not be changed.';
  }
  
  $form['space']['description'] = array(
    '#title' => t('Description'),
    '#description' => t('A brief description of what this space is used for'),
    '#type' => 'textfield',
    '#default_value' => $space->description,
  );
  
  $form['space']['category'] = array(
    '#title' => t('Admin category'),
    '#description' => t('Tag this space with a category for easier reference on the spaces overview page.'),
    '#type' => 'textfield',
    '#default_value' => $space->category,
    '#autocomplete_path' => 'adtoma_fusion_space_category_js',
  );
  
  $form['space']['disabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disabled'),
    '#default_value' => $space->disabled,
  );
  
  $form['space']['block'] = array(
    '#type' => 'checkbox',
    '#title' => t('Expose as block'),
    '#default_value' => $space->block,
  );
  
  $form['space']['exclude'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Excluded configurations'),
    '#description' => t('Select any configurations that this space should not appear on.'),
    '#options' => array(),
    '#default_value' => $space->exclude,
  );
  foreach (adtoma_fusion_config_load_multiple() as $config) {
    $form['space']['exclude']['#options'][$config->machine_name] = $config->label;
  }
    
  $form['buttons'] = array(
    '#weight' => 10,
    'save' => array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#submit' => array('adtoma_fusion_form_space_submit'),
    ),
  );
  
  return $form;
}

/**
 * Validation handler for name
 */
function adtoma_fusion_form_space_name_validate($element, $form, $form_state) {
  if (adtoma_fusion_space_name_exists($element['#value'])) {
    form_set_error(implode('][', $element['#parents']), t('The space name %space has already been used. Please try again with a unique name.', array('%space' => $element['#value'])));
  }
}

/**
 * Submit handler for shared space form
 */
function adtoma_fusion_form_space_submit($form, &$form_state) {
  $space = (object) $form_state['values']['space'];
  if (adtoma_fusion_space_save($space)) {
    drupal_set_message(t('Successfully saved %space!', array('%space' => $space->name)));
  }
  
  $form_state['redirect'] = 'admin/adtoma/fusion/space';
}

/**
 * Autocomplete callback for space category field
 */
function adtoma_fusion_space_category_js($string) {
  $matches = array();
  
  $q = db_select('adtoma_fusion_space', 'space');
  $q->fields('space', array('category'));
  $q->addExpression('COUNT(*)', 'count');
  $q->groupBy('space.category');
  $q->condition('space.category', db_like($string) .'%', 'LIKE');
  $rs = $q->execute();
  foreach ($rs->fetchAll() as $row) {
    $matches[$row->category] = $row->category .' ('. $row->count .')';
  }
  
  drupal_json_output($matches);
}

/**
 * Delete form for spaces
 */
function adtoma_fusion_form_delete_space($form, &$form_state, $space) {
  $form['adtoma_fusion_space_id'] = array(
    '#type' => 'hidden',
    '#value' => $space->id,
  );
  $form['#submit'] = array('adtoma_fusion_form_delete_space_submit');
  
  $question = t('Are you sure you wish to delete space %space?', array('%space' => $space->name));
  $arg = arg();
  $path = implode('/', array_slice($arg, 0, count($arg) - 2));
  return confirm_form($form, $question, $path);
}

/**
 * Submission handler for space delete form
 */
function adtoma_fusion_form_delete_space_submit($form, &$form_state) {
  $space = adtoma_fusion_space_load($form_state['values']['adtoma_fusion_space_id']);
  adtoma_fusion_space_delete($space->id);
  drupal_set_message(t('Successfully deleted space %space.', array('%space' => $space->name)));
  $form_state['redirect'] = 'admin/adtoma/fusion/space';
}

